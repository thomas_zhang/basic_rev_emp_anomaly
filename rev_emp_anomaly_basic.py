"""
Basic anomaly detection employing isolation forest and linear regression.

Written by Thomas Zhang: thomas@owler-inc.com on Oct 13, 2017.
-- testing with mongo connection Oct 19, 2017
-- cleaning up for production Oct 26, 2017
-- further preparation for production Nov 2, 2017
"""

import pandas as pd
import numpy as np
from scipy.stats import linregress
from sklearn.ensemble import IsolationForest
from pymongo import MongoClient
from pymongo.read_preferences import ReadPreference
import config

configData = config.getConfig('config.properties')
C = float(configData['REV_EMP_C'])
SIZE = float(configData['REV_EMP_SIZE'])
REVENUE = float(configData['REV_EMP_REVENUE'])


def regression_anomaly(df, c=C, size=SIZE, revenue=REVENUE):
    """ regression analysis using linear regression"""
    # outliers
    x, y = df['total_employees'].values, df['total_revenue'].values
    X = np.asarray([x, y]).T
    clf = IsolationForest()
    clf.fit(X)
    ypred = clf.predict(X)
    xlist = x[ypred == 1]
    ylist = y[ypred == 1]
    outliers = set([i for i in range(len(x)) if ypred[i] == -1])

    # linear fit
    fit = linregress(xlist, ylist)
    slope, intercept = fit.slope, fit.intercept

    y = y - intercept
    anomalies = set([])
    ratio_outlier = set([])
    for i in range(len(x)):
        pred = x[i] * slope
        diff = y[i] - pred
        ratio = abs(y[i] / pred)
        if diff > 0 and ratio > c:
            anomalies.add(i)
            if x[i] > size or y[i] > revenue:
                ratio_outlier.add(i)
        elif diff < 0 and ratio < 1. / c:
            anomalies.add(i)
            if x[i] > size or y[i] > revenue:
                ratio_outlier.add(i)
    res = anomalies & outliers
    res = res | ratio_outlier
    res = set([df['_id'].values[e] for e in res])
    return res


def anomaly_detection(company, com_info, cg):
    """ main anomaly detection function"""
    # prepare dataframe for anomaly detection
    lst = [company]
    neighbors = cg.find({'_id': company}).next()
    for e in neighbors['competitors']:
        lst.append(int(e['competitor_id']))
    n = len(lst)

    # insert revenue and employee information if available
    revenue = np.nan * np.ones(n)
    employee = np.nan * np.ones(n)
    for i in range(n):
        com = lst[i]
        data = com_info.find({'_id': com}).next()
        try:
            revenue[i] = int(data['total_revenue'])
        except:
            continue
        try:
            employee[i] = int(data['total_employees'])
        except:
            continue
    print('Data frame complete')

    # return the results that contains both emloyee and revenue
    df = pd.DataFrame()
    df['_id'] = lst
    df['total_revenue'] = revenue
    df['total_employees'] = employee
    df = df[df['total_revenue'].notnull()]
    df = df[df['total_employees'].notnull()]

    anomaly = regression_anomaly(df)
    return anomaly


def main():
    """ example usage"""
    print('building connection...')
    port = '6080'
    client = MongoClient('54.183.128.146:' + port, read_preference=ReadPreference.SECONDARY)
    db = client.owler
    db.authenticate('dqmetrics', '0w%3r$#@')
    collection = db['company']
    com_info = collection

    port = '6086'
    client = MongoClient('54.183.128.146:' + port, read_preference=ReadPreference.SECONDARY)
    db = client.owler_contribution
    db.authenticate('dqmetrics', '0w%3r$#@')
    collection = db['company_competitor_graph']
    cg = collection
    print('connection successful.')

    # get complete list of companies in CG.
    company_lst = cg.distinct('_id')
    anomaly_set = set()
    for company in company_lst:
        anomaly = anomaly_detection(company, com_info, cg)
        anomaly_set.union(anomaly)

    # saving and uploading, maybe add in revenue, name and so on... leave for future
    df = pd.DataFrame()
    df['company_id'] = list(anomaly_set)
    df.to_csv('anomaly.csv', index=False)


if __name__ == "__main__":
    main()
